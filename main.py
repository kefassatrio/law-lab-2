from typing import Optional

from fastapi import FastAPI, Form

from pydantic import BaseModel

class NumberItems(BaseModel):
    num_1: int
    num_2: int

app = FastAPI()

def is_the_same_number(num_1: int, num_2:int):
    return {"is_the_same_number": num_1 == num_2}

@app.get("/is-the-same-number/{num_1}/{num_2}")
def get_is_the_same_number(num_1: int, num_2: int):
    return is_the_same_number(num_1, num_2)

@app.post("/is-the-same-number/")
async def post_1_is_the_same_number(num_1: int = Form(...), num_2: int = Form(...)):
    return is_the_same_number(num_1, num_2)

@app.post("/is-the-same-number/")
async def post_2_is_the_same_number(payload: NumberItems):
    return is_the_same_number(payload.num_1, payload.num_2)
